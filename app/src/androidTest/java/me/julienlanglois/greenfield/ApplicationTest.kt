package me.julienlanglois.greenfield

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.matcher.ViewMatchers.withClassName
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.widget.EditText
import me.julienlanglois.greenfield.app.main.MainActivity
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * [Testing Fundamentals](http://d.android.com/tools/testing/testing_android.html)
 */
@RunWith(AndroidJUnit4::class)
@LargeTest
class ApplicationTest {
    @JvmField @Rule val activityRule = ActivityTestRule(MainActivity::class.java)

    @Test fun basicTest() {
        // TODO this is awkward
        onView(withClassName(equalTo(EditText::class.java.name))).perform(typeText("joe"))
        onView(withText(R.string.find)).perform(click())
    }
}
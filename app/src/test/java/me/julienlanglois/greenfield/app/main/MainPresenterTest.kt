package me.julienlanglois.greenfield.app.main

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import me.julienlanglois.greenfield.api.UserApi
import me.julienlanglois.greenfield.model.User
import okhttp3.HttpUrl
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.*
import java.util.*

class MainPresenterTest {
    val user = User("joe@nowhere.com", "Joe Blow", HttpUrl.parse("https://nowhere.com/profile.jpg"))
    val users = object: UserApi {
        override fun byId(id: String): Single<User> =
            if (id == "joe") Single.just(user) else Single.error(NoSuchElementException())
    }
    val mainThread = Schedulers.trampoline()
    val view: MainUseCase.View = Mockito.mock(MainUseCase.View::class.java)

    @Test fun basicTest() {
        val presenter = MainUseCase.Presenter(users, mainThread)
        presenter.register(view)
        presenter.findUser("joe")
        verify(view).updateUserDetails(user)
        verify(view, never()).clearUserDetails()
        presenter.findUser("job")
        verify(view).updateUserDetails(user)
        verify(view).clearUserDetails()
    }
}

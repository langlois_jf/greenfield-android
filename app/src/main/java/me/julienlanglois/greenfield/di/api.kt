package me.julienlanglois.greenfield.di

import com.github.salomonbrys.kotson.registerTypeAdapter
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Component
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import me.julienlanglois.greenfield.BuildConfig
import me.julienlanglois.greenfield.app.main.MainActivity
import me.julienlanglois.greenfield.api.UserApi
import me.julienlanglois.greenfield.model.User
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.IllegalArgumentException
import javax.inject.Qualifier
import javax.inject.Singleton

@Singleton @Module class ApiModule {
    @Provides @ApiRoot fun apiRoot(): HttpUrl = HttpUrl.parse(BuildConfig.API_BASE_URL)
    @Provides fun gson(): Gson =
            GsonBuilder()
                    .registerTypeAdapter<User> {
                        deserialize {
                            try {
                                val jsonObject = it.json.asJsonObject
                                val email = jsonObject.get("email").asString
                                val name = jsonObject.get("name").asString
                                val photo = jsonObject.get("photo").asString
                                User(email, name, HttpUrl.parse(photo))
                            } catch (e: Throwable) {
                                throw IllegalArgumentException("Unexpected JSON for Test", e)
                            }
                        }
                        serialize {
                            val result = JsonObject()
                            result.addProperty("email", it.src.email)
                            result.addProperty("name", it.src.name)
                            result.addProperty("photo", it.src.photo.toString())
                            result
                        }
                    }
                    .create()

    @Provides fun retrofit(
            httpClient: OkHttpClient,
            gson: Gson,
            @ApiRoot baseUrl: HttpUrl,
            @NetworkThread networkThread: Scheduler): Retrofit = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(httpClient)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(networkThread))
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()

    @Provides fun newsApi(retrofit: Retrofit): UserApi = retrofit.create(UserApi::class.java)
}

@Qualifier @Retention(AnnotationRetention.RUNTIME) annotation class ApiRoot
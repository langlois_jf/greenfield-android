package me.julienlanglois.greenfield.di

import dagger.Component
import me.julienlanglois.greenfield.app.main.MainFragment
import javax.inject.Scope

@Scope annotation class FragmentScope

@FragmentScope @Component(modules = arrayOf(ApiModule::class, AppModule::class, ThreadingModule::class, NetworkModule::class))
interface FragmentComponent {
    fun inject(fragment: MainFragment)
}

package me.julienlanglois.greenfield.di

import com.squareup.leakcanary.RefWatcher
import dagger.Module
import dagger.Provides
import me.julienlanglois.greenfield.MainApplication
import me.julienlanglois.greenfield.util.eventbus.Event
import me.julienlanglois.greenfield.util.eventbus.RxEventBus
import javax.inject.Singleton

@Singleton @Module class AppModule(val application: MainApplication, val refWatcher: RefWatcher) {
    @Provides fun application(): MainApplication = application
    @Provides fun refWatcher(): RefWatcher = refWatcher
    @Provides fun eventBus(): RxEventBus<Event> = RxEventBus()
}


package me.julienlanglois.greenfield.di

import dagger.Module
import dagger.Provides
import me.julienlanglois.greenfield.BuildConfig
import me.julienlanglois.greenfield.MainApplication
import okhttp3.Cache
import okhttp3.OkHttpClient
import java.io.File
import javax.inject.Singleton

@Singleton @Module class NetworkModule {
    @Provides fun httpCache(application: MainApplication): Cache =
            Cache(File(application.cacheDir, "http-cache"), BuildConfig.API_HTTP_CACHE_SIZE)

    @Provides fun httpClient(cache: Cache): OkHttpClient =
            OkHttpClient.Builder()
                    .cache(cache)
                    .build()

}
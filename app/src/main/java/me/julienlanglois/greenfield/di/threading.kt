package me.julienlanglois.greenfield.di

import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Qualifier
import javax.inject.Singleton

@Qualifier annotation class MainThread
@Qualifier annotation class NetworkThread

@Singleton @Module class ThreadingModule {
    @Provides @MainThread fun mainThread(): Scheduler = AndroidSchedulers.mainThread()
    @Provides @NetworkThread fun networkThread(): Scheduler = Schedulers.io()
}

package me.julienlanglois.greenfield.api

import io.reactivex.Single
import me.julienlanglois.greenfield.model.User
import retrofit2.http.GET
import retrofit2.http.Path

interface UserApi {
    @GET("user/{id}.json") fun byId(@Path("id") id: String): Single<User>
}
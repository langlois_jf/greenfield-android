package me.julienlanglois.greenfield.app.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.common.GoogleApiAvailability
import me.julienlanglois.greenfield.BuildConfig

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            val fragment = MainFragmentBuilder("2141").build()
            supportFragmentManager.beginTransaction()
                    .add(android.R.id.content, fragment, MainFragment.TAG)
                    .commit()
        }

        if (!BuildConfig.DEBUG) {
            GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(this)
        }
    }
}

package me.julienlanglois.greenfield.app.main

import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import me.julienlanglois.greenfield.BuildConfig
import me.julienlanglois.greenfield.api.UserApi
import me.julienlanglois.greenfield.di.MainThread
import me.julienlanglois.greenfield.model.User
import timber.log.Timber
import java.util.*
import javax.inject.Inject

object MainUseCase {
    class Presenter @Inject constructor(val users: UserApi, @MainThread val mainThread: Scheduler): me.julienlanglois.greenfield.util.mvp.Presenter<View> {
        var view: View = NulView
        var subscription: Disposable? = null

        fun findUser(userId: String) {
            subscription = users.byId(userId)
                    .observeOn(mainThread)
                    .subscribe(view::updateUserDetails, {
                        if (it is NoSuchElementException) {
                            view.clearUserDetails()
                        } else {
                            Timber.e(it)
                        }
                    })
        }

        override fun register(view: View) {
            this.view = view
        }

        override fun destroy() {
            subscription?.dispose()
            subscription = null
            view = NulView
        }
    }

    private object NulView: View {
        override fun updateUserDetails(user: User) {
            if (BuildConfig.DEBUG) {
                throw IllegalStateException()
            }
            Timber.i("Trying to update user ${user} on nul view instance")
        }

        override fun clearUserDetails() {
            if (BuildConfig.DEBUG) {
                throw IllegalStateException()
            }
            Timber.i("Trying to clear user on nul view instance")
        }
    }

    interface View : me.julienlanglois.greenfield.util.mvp.View {
        fun updateUserDetails(user: User)
        fun clearUserDetails()
    }
}

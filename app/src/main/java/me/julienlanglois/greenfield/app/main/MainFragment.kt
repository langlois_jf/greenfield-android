package me.julienlanglois.greenfield.app.main

import android.content.Context
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.crash.FirebaseCrash
import com.hannesdorfmann.fragmentargs.FragmentArgs
import com.hannesdorfmann.fragmentargs.annotation.Arg
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs
import com.squareup.leakcanary.RefWatcher
import com.trello.rxlifecycle2.components.support.RxFragment
import me.julienlanglois.greenfield.MainApplication
import me.julienlanglois.greenfield.R
import me.julienlanglois.greenfield.di.DaggerFragmentComponent
import me.julienlanglois.greenfield.model.User
import me.julienlanglois.greenfield.util.ui.clear
import me.julienlanglois.greenfield.util.ui.load
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.textInputLayout
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.drawerLayout
import java.lang.RuntimeException
import javax.inject.Inject

@FragmentWithArgs
class MainFragment: RxFragment(), MainUseCase.View {
    lateinit private var result: TextView
    lateinit private var testImage: ImageView
    lateinit private var toolbar: Toolbar
    lateinit private var drawerLayout: DrawerLayout

    @field:Inject lateinit var refWatcher: RefWatcher
    @field:Inject lateinit var presenter: MainUseCase.Presenter

    @field:Arg lateinit var id: String

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        DaggerFragmentComponent.builder()
                .appModule(MainApplication.from(context!!).appModule)
                .build()
                .inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FragmentArgs.inject(this)
        setHasOptionsMenu(true)
        presenter.register(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View =
        UI {
            coordinatorLayout {
                fitsSystemWindows = true

                drawerLayout = drawerLayout {

                    verticalLayout {
                        toolbar = toolbar()
                        val idEdit = textInputLayout {
                            editText() {
                                hintResource = R.string.user_id_hint
                                singleLine = true
                                imeOptions = EditorInfo.IME_ACTION_DONE
                            }
                        }
                        button(text = R.string.find) {
                            onClick { presenter.findUser(idEdit.editText!!.text.toString()) }
                        }
                        result = textView()
                        testImage = imageView {
                            scaleType = ImageView.ScaleType.FIT_CENTER
                        }.lparams {
                            width = matchParent
                            height = wrapContent
                        }
                    }.lparams { width = matchParent }

                    frameLayout {
                        verticalLayout {
                            button(text = "start") {
                                onClick {
                                    drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
                                    drawerLayout.closeDrawer(Gravity.START)
                                }
                            }
                        }
                        backgroundColor = 0x55850045
                    }.lparams {
                        width = matchParent
                        height = matchParent
                        gravity = Gravity.START
                        marginEnd = dip(-65)
                    }

                }.lparams {
                    width = matchParent
                    height = matchParent
                }
            }
        }.view

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        drawerLayout.openDrawer(Gravity.START, false)
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN)
    }

    override fun onStart() {
        super.onStart()
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
    }

    override fun onDestroy() {
        presenter.destroy()
        super.onDestroy()
        refWatcher.watch(this)
    }

    override fun updateUserDetails(user: User) {
        result.textColor = ResourcesCompat.getColor(ctx.resources, R.color.text, ctx.theme)
        result.text = "${user.email}, ${user.name}"
        testImage.load(user.photo)
    }

    override fun clearUserDetails() {
        result.textColor = ResourcesCompat.getColor(ctx.resources, R.color.error, ctx.theme)
        result.text = "Not found"
        testImage.clear()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_crash_report -> FirebaseCrash.report(RuntimeException("Meow"))
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val TAG = "MainFragment"
    }
}

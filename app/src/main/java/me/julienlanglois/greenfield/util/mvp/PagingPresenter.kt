package me.julienlanglois.greenfield.util.mvp

interface PagingPresenter<in V: View>: Presenter<V> {
    fun fetchMore()
}
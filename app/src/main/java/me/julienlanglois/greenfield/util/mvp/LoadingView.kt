package me.julienlanglois.greenfield.util.mvp

interface LoadingView: View {
    fun startLoading()
    fun stopLoading()
}
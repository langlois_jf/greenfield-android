package me.julienlanglois.greenfield.util.eventbus

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

class RxEventBus<T> {
    private val subject: Subject<T> = PublishSubject.create<T>().toSerialized()

    fun post(event: T) = subject.onNext(event)
    fun <U: T> register(clazz: Class<U>): Observable<U> = subject.ofType(clazz)
    fun registered(): Boolean = subject.hasObservers()
}

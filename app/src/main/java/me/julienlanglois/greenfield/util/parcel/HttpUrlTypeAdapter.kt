package me.julienlanglois.greenfield.util.parcel

import android.os.Parcel
import okhttp3.HttpUrl
import paperparcel.TypeAdapter

@Suppress("unused")
class HttpUrlTypeAdapter: TypeAdapter<HttpUrl> {
    override fun writeToParcel(url: HttpUrl?, parcel: Parcel, flags: Int) {
        parcel.writeString(url?.toString() ?: "")
    }

    override fun readFromParcel(parcel: Parcel): HttpUrl? = HttpUrl.parse(parcel.readString())
}

package me.julienlanglois.greenfield.util.mvp

interface Presenter<in V: View> {
    fun register(view: V)
    fun destroy()
}
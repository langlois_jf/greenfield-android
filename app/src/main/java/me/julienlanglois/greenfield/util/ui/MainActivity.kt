package me.julienlanglois.greenfield.util.ui

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import okhttp3.HttpUrl
import org.jetbrains.anko.image

fun ImageView.load(src: HttpUrl): Unit {
    val loader = Glide.with(context).load(src.toString())
    if (scaleType == ImageView.ScaleType.CENTER_CROP) {
        loader.apply(RequestOptions().centerCrop())
    } else if (scaleType == ImageView.ScaleType.FIT_CENTER) {
        loader.apply(RequestOptions().fitCenter())
    }
    loader.into(this)
}

fun ImageView.clear(): Unit {
    Glide.with(this).clear(this)
    image = null
}
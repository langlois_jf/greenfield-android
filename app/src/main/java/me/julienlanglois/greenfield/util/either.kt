package me.julienlanglois.greenfield.util

sealed class Either<out L, out R> {
    data class Left<out L, out R>(val value: L): Either<L, R>() {
        override val isLeft = true
        override val isRight = false
        override fun traverse(left: (L) -> Unit, right: (R) -> Unit) {
            left(value)
        }
    }

    data class Right<out L, out R>(val value: R): Either<L, R>() {
        override val isLeft = false
        override val isRight = true
        override fun traverse(left: (L) -> Unit, right: (R) -> Unit) {
            right(value)
        }
    }

    abstract fun traverse(left: (L) -> Unit, right: (R) -> Unit)
    abstract val isLeft: Boolean
    abstract val isRight: Boolean

    companion object {
        fun <L, R> left(value: L): Either<L, R> = Left(value)
        fun <L, R> right(value: R): Either<L, R> = Right(value)
    }
}

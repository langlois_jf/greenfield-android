package me.julienlanglois.greenfield.util.parcel

import paperparcel.Adapter
import paperparcel.ProcessorConfig

@Suppress("unused")
@ProcessorConfig(adapters = arrayOf(Adapter(HttpUrlTypeAdapter::class)))
interface PaperParcelConfig

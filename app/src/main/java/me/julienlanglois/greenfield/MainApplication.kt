package me.julienlanglois.greenfield

import android.app.Application
import android.content.Context
import android.support.v4.app.Fragment
import com.squareup.leakcanary.LeakCanary
import me.julienlanglois.greenfield.di.AppModule
import timber.log.Timber
import java.lang.IllegalStateException

class MainApplication: Application() {
    lateinit var appModule: AppModule
        get
        private set

    override fun onCreate() {
        super.onCreate()

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return
        }
        val refWatcher = LeakCanary.install(this)

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        appModule = AppModule(this, refWatcher)
    }

    companion object {
        fun from(context: Context): MainApplication {
            val appContext = context.applicationContext
            if (appContext is MainApplication) {
                return appContext
            } else {
                throw IllegalStateException("Application appContext is not MainApplication")
            }
        }

        fun from(fragment: Fragment): MainApplication = from(fragment.activity)
    }
}
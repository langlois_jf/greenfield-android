package me.julienlanglois.greenfield.model

import me.julienlanglois.greenfield.model.trait.Model
import okhttp3.HttpUrl

data class User(val email: String, val name: String, val photo: HttpUrl): Model
